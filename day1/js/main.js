WebsiteModel = Backbone.Model.extend({
    defaults: {
     name: "An unknown website",
     barcode: -1   
    },

    initialize: function(){
        
    }
});

SandwichModel = Backbone.Model.extend({
    defaults: {
        bread: "wheat"    
    },

    initialize: function(){ 
        this.bindEvents();
    },

    bindEvents: function(){ 
        this.on("change:bread", function(model){
            var my_new_bread = model.get("bread");            
            alert("Hey! The bread changed to: " + my_new_bread);
        });
    }

});

$(document).ready(function(){
    //var page_1 = new WebsiteModel();    
    //alert("Page 1 title is "+ page_1.get("name")+" and its barcode is: "+page_1.get("barcode"));
    var my_sandwich = new SandwichModel();
    my_sandwich.set({bread: "jalapeno"});
});